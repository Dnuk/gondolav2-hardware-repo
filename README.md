**Repozytorium hardware'u Gondoli V2

**Zawiera wszystkie schematy ideowe oraz pcb podzespołów gondoli w formacie programu Eagle.
Dodatkowo pliki projektowe mini-sondy budowanej na zlecenie

**Zawiera zgrubną dokumentację z pierwszych prac nad sprzętem (praca dyplomowa)

-- Wszystkie materiały udostępniane są na licencji open software/hardware do użytku niekomercyjnego

-------------------------------------------------------------------------
**Linki do repozytoriów kodu:

https://bitbucket.org/Dnuk/gnss_gondola2560/overview

https://bitbucket.org/Dnuk/gondola-v2-pikacz/overview

https://bitbucket.org/Dnuk/watchdog_tiny10/overview

https://bitbucket.org/Dnuk/odcinacz/overview

https://bitbucket.org/Dnuk/sonda_microsoft/overview

https://bitbucket.org/Dnuk/gsm-tracker-fibo/overview

-------------------------------------------------------------------------
**Oprogramowanie Stacji Naziemnej -> StratoTerminal v2.0 [nowy, mniej stabilny] 
By Fabian Oleksiuk

https://bitbucket.org/FabianoV/stratoterminal2

----------------------------------

**StratoTerminal v1.1 -> [uzupełnić]

------------------------------------

Kontakt do nas oraz więcej materiałów tutaj:   https://www.facebook.com/DNFsystems/